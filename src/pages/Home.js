import React from 'react'
import { Select2 } from '../components'

const options = [
    { name: 'Nguyen' },
    { name: 'thang' },
    { name: 'hai' }
];

const Home = () => {
    return (
        <div className='home' style={{ padding: 32 }}>
            <h3>Home Page</h3>
            <Select2
                options={options}
                getOptionLabel={option => option.name}
                filterOption={['name']}
            />
        </div>
    )
}

export default Home;