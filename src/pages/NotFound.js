import React from 'react'

const NotFound = () => {
    return (
        <h3>Not Found 404.</h3>
    )
}

export default NotFound;