export const getToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
        return token;
    }
    return null;
};

export const setToken = (token) => {
    if (token) {
        localStorage.setItem("token", token);
    }
};