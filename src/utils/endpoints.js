const API_URL = 'https://squadware-ui-api.herokuapp.com';

// login endpoints
export const LOGIN = API_URL + '/login';

// employee endpoints
export const EMPLOYEES = API_URL + '/employees'