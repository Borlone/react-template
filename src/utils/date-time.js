const monthString = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'Octorber',
    'November',
    'December',
];

export function getYearList(startYear = 2000) {
    const currentYear = new Date().getFullYear();
    const years = [];
    while (startYear <= currentYear + 50) {
        years.push(startYear++);
    }
    return years;
}

export function getDaysInMonth(year, month) {
    // month is in the range 1..12
    const firstDayInFirstWeek = new Date(year, month - 1, 1).getDay();
    const lastDay = new Date(year, month, 0).getDate();

    const days = [];
    for (let i = 0; i < firstDayInFirstWeek; i++) {
        days[i] = ''
    }

    for (let i = 1; i <= lastDay; i++) {
        days.push(new Date(year, month - 1, i));
    }

    return days;
}

export function isEqual(d1, d2) {
    if (d1 instanceof Date && d2 instanceof Date) {
        return d1.getFullYear() === d2.getFullYear() &&
               d1.getMonth() === d2.getMonth() &&
               d1.getDate() === d2.getDate()
    }

    return false;
}

export function getStringDate(d) {
    if (d instanceof Date) {
        return monthString[d.getMonth()] + ' ' + d.getFullYear()
    }

    return null;
}