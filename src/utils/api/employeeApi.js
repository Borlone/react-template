import { get, post } from './axios';
import { EMPLOYEES } from '../endpoints';

const employeeApi = {
    getEmployees: (page) => {
        return get(EMPLOYEES + `/page/${page}`);
    },

    getEmployee: (id) => {
        return get(EMPLOYEES + `/${id}`);
    },

    postEmployee: (data) => {
        return post(EMPLOYEES, data);
    }
}

export default employeeApi;