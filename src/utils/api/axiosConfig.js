import axios from "axios";
import { getToken } from '../helpers';

const instance = axios.create({
    headers: {
        Authorization: `Bearer ${getToken()}`
    }
});

instance.interceptors.request.use(function(config) {
    // Do something before request is sent
    return config;
}, function(error) {
    // Do something with request error
    return Promise.reject(error);
});

export default instance;