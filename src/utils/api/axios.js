import axios from './axiosConfig';

export async function get(url, params) {
    return await axios.get(url, { ...params })
        .catch((e) => { return e; });
}

export async function post(url, body, params) {
    return await axios.post(url, body, { ...params })
        .catch((e) => { return e; });
}

export async function patch(url, body, params) {
    return await axios.patch(url, body, { ...params })
        .catch((e) => { return e; });
}

export async function put(url, body, params) {
    return await axios.put(url, body, { ...params })
        .catch((e) => { return e; });
}

export async function deleteRequest(url, params) {
    return await axios.delete(url, { ...params })
        .catch((e) => { return e; });
}