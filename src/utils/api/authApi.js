import axios from "axios";
import { LOGIN } from '../endpoints';

async function postLogin(url, data) {
    return await axios.post(url, data)
        .catch((e) => { return e; });
}

const authApi = {
    login: (data) => {
        return postLogin(LOGIN, data);
    }
}

export default authApi;