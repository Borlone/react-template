import React from 'react'
import { Header, Menu, Footer } from './components'

const MainLayout = ({ children }) => {
    return (
        <div className='app'>
            <Header />
            <Menu />
            {children}
            <Footer />
        </div>
    )
}

export default MainLayout;