import React from 'react'
import { Switch } from 'react-router-dom'
import { RouteWithLayout } from './components'
import { MainLayout, MinimalLayout } from './layout'
import { Home, About, NotFound } from './pages'

const Routes = () => {
    return (
        <Switch>
            <RouteWithLayout
                component={Home}
                exact
                layout={MainLayout}
                path="/"
            />
            <RouteWithLayout
                component={About}
                exact
                layout={MainLayout}
                path="/about"
            />
            <RouteWithLayout
                component={NotFound}
                exact
                layout={MinimalLayout}
                path="*"
            />
        </Switch>
    )
}

export default Routes;