import { useEffect, useRef } from 'react';

const useResize = (minWidth = 20, maxWidth = 500) => {

    const div = useRef();
    const min = useRef(minWidth);
    const max = useRef(maxWidth);
    const currentWidth = useRef();
    const oldX = useRef();

    const getCurrentCoordinate = (event) => {
        oldX.current = event.clientX;
        document.addEventListener('mousemove', updateCoordinate);
        document.addEventListener('mouseup', getNewCoordinate);
    }
    const updateCoordinate = (event) => {
        const newWidth = currentWidth.current + event.clientX - oldX.current;
        if (newWidth >= min.current && newWidth <= max.current) {
            div.current.style.width = newWidth + 'px';
        }
    }
    const getNewCoordinate = (event) => {
        currentWidth.current += event.clientX - oldX.current;
        document.removeEventListener('mousemove', updateCoordinate);
    }

    useEffect(() => {
        if (div && div.current) {
            const parent = div.current;

            max.current = max.current < min.current ? min.current : max.current;

            let width = parent.offsetWidth;
            width = width < min.current ? min.current : width;
            width = width > max.current ? max.current : width;
            currentWidth.current = width;

            // style parent
            parent.style.width = width + 'px';
            parent.style.position = 'relative';

            // style resize div
            const resize = document.createElement('div');
            resize.style.position = 'absolute';
            resize.style.right = '0';
            resize.style.cursor = 'col-resize';
            resize.style.width = '3px';
            resize.style.height = '100%';
            parent.appendChild(resize);

            // add event for resizing
            resize.addEventListener('mousedown', getCurrentCoordinate);

            return () => {
                resize.removeEventListener('mousedown', getCurrentCoordinate);
                document.removeEventListener('mouseup', getNewCoordinate);
            }
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    return div;
};

export default useResize;