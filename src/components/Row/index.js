import React, { Children, isValidElement, cloneElement } from 'react'
import PropTypes from 'prop-types'
import styles from './Row.module.css'

const Row = ({ children, spacing, className, style }) => {
    return (
        <div className={`${styles.row} ${styles[`spacing-${spacing}`]} ${className}`}
            style={style}>
            {Children.map(children, child => {
                if (isValidElement(child)) {
                    return cloneElement(child, { spacing })
                }
                return child;
            })}
        </div>
    )
}

Row.propTypes = {
    spacing: PropTypes.number,
    className: PropTypes.string,
    style: PropTypes.object
}

Row.defaultProps = {
    spacing: 1,
    className: ''
}

export default Row;