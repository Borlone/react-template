import React from 'react'
import PropTypes from 'prop-types'
import styles from './Col.module.css'

const Col = ({ xs, sm, md, lg, xl, spacing, className, style, children }) => {

    const colStyle = `${styles.col} `;
    const xsStyle = xs ? styles[`xs-${xs}`] + ' ' : '';
    const smStyle = sm ? styles[`sm-${sm}`] + ' ' : '';
    const mdStyle = md ? styles[`md-${md}`] + ' ' : '';
    const lgStyle = lg ? styles[`lg-${lg}`] + ' ' : '';
    const xlStyle = xl ? styles[`xl-${xl}`] + ' ' : '';
    const spacingStyle = styles[`spacing-${spacing}`];
    const classStyle = className ? `${className} ` : '';

    const classes = [colStyle, xsStyle, smStyle, mdStyle, lgStyle, xlStyle, spacingStyle, classStyle].join('');

    return (
        <div className={classes} style={style}>
            {children}
        </div>
    )
}

Col.propTypes = {
    spacing: PropTypes.oneOf([1, 2, 3]),
    className: PropTypes.string,
    style: PropTypes.object,
    xs: PropTypes.number,
    sm: PropTypes.number,
    md: PropTypes.number,
    lg: PropTypes.number,
    xl: PropTypes.number
}

Col.defaultProps = {
    spacing: 1,
    className: ''
}

export default Col;