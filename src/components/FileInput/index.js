import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './FileInput.module.css';

const FileInput = ({ className, accept, limitSize, onChange }) => {

    const { file_input, choose_file, file_thumbnail, file_name } = styles;

    const [file, setFile] = useState({ name: '', thumbnail: null });

    const handleChangeFile = event => {
        const file = event.target.files[0];
        if (file) {
            setFile({
                name: file.name,
                thumbnail: file.type.includes('image/') ? URL.createObjectURL(file) : null
            });
            onChange(file)
        }
    }

    return (
        <div className={`${file_input} ${className}`}>
            <label className={choose_file} htmlFor='file_input'>Choose the file</label>
            {file.thumbnail && (
                <img className={file_thumbnail}
                    src={file.thumbnail}
                    alt='thumbnail'
                />
            )}
            <span className={file_name}>{file.name}</span>
            <input
                style={{ display: 'none' }}
                id='file_input'
                type='file'
                accept={accept}
                onChange={handleChangeFile}
            />
        </div>
    )
}

FileInput.propTypes = {
    className: PropTypes.string,
    accept: PropTypes.string,
    limitSize: PropTypes.number,
    onChange: PropTypes.func
}

FileInput.defaultProps = {
    className: '',
    accept: 'all',
    limitSize: 5242880,
    onChange: () => { }
}

export default FileInput;