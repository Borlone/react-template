import React from 'react'
import PropTypes from 'prop-types'
import styles from './Grid.module.css'

const Grid = ({ cols, gap, className, style, children }) => {

    const classes = [
        styles['grid-container'], 
        styles[`grid-cols-${cols}`],
        styles[`grid-gap-${gap}`],
        className
    ].join(' ');

    return (
        <div className={classes} style={style}>
            {children}
        </div >
    )
}

Grid.propTypes = {
    cols: PropTypes.number,
    gap: PropTypes.number,
    className: PropTypes.string,
    style: PropTypes.object
}

Grid.defaultProps = {
    cols: 4,
    gap: 1,
    className: ''
}

export default Grid;