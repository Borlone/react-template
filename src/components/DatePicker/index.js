import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import MonthPicker from './components/MonthPicker'
import YearPicker from './components/YearPicker'
import { getYearList, getDaysInMonth, isEqual } from '../../utils/date-time'

import styles from './DatePicker.module.css'

let mounted = false;
const DatePicker = ({ value }) => {

    const years = getYearList();

    const [selectedDate, setSelectedDate] = useState(value || new Date());
    const [days, setDays] = useState([]);

    const selectedYear = selectedDate.getFullYear();
    const selectedMonth = selectedDate.getMonth();
    const now = selectedDate.toDateString().split(' ');

    const onChangeDate = (date) => {
        setSelectedDate(date);
    }
    const changePrevMonth = () => {

    }
    const changeNextMonth = () => {
        
    }

    useEffect(() => {
        mounted = true;

        if (mounted) {
            setDays(getDaysInMonth(selectedYear, selectedMonth + 1));
        }

        return () => mounted = false;
    }, [selectedYear, selectedMonth])

    return (
        <div className={styles.datepicker_modal}>
            <div className={styles.datepicker}>
                <div className={styles.datenow}>
                    <p className={styles.year}>
                        <span>{now[3]}</span></p>
                    <p className={styles.date_string}><span>
                        {now[0] + ', ' + now[1] + ' ' + parseInt(now[2])}
                    </span></p>
                </div>
                <div className={styles.calendar}>
                    <MonthPicker
                        days={days}
                        selectedDate={selectedDate}
                        onChangeDate={onChangeDate}
                        prevMonth={changePrevMonth}
                        nextMonth={changeNextMonth}
                    />
                </div>
            </div>
        </div>
    )
}

DatePicker.propTypes = {
    value: PropTypes.instanceOf(Date),
}

DatePicker.defaultProps = {
    value: new Date(),
}

export default DatePicker;