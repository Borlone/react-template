import React from 'react'
import PropTypes from 'prop-types'
import { isEqual, getStringDate } from '../../../utils/date-time'
import nextArrow from '../../../assets/images/next-arrow.svg'
import prevArrow from '../../../assets/images/prev-arrow.svg'
import styles from '../DatePicker.module.css'

const MonthPicker = ({ days, selectedDate, onChangeDate, prevMonth, nextMonth }) => {
    const now = selectedDate.toDateString().split(' ');
    return (
        <>
            <div className={styles.selectedDate}>
                <img src={prevArrow} alt='prev-icon' onClick={prevMonth} />
                <h6>{getStringDate(selectedDate)}</h6>
                <img src={nextArrow} alt='next-icon' onClick={nextMonth} />
            </div>
            <div className={styles.days}>
                <div className={styles.dayInWeek}><span>Su</span></div>
                <div className={styles.dayInWeek}><span>Mo</span></div>
                <div className={styles.dayInWeek}><span>Tu</span></div>
                <div className={styles.dayInWeek}><span>We</span></div>
                <div className={styles.dayInWeek}><span>Th</span></div>
                <div className={styles.dayInWeek}><span>Fr</span></div>
                <div className={styles.dayInWeek}><span>Sa</span></div>
                {days.map((day, i) => (
                    <div key={`day-${i}`} className={styles.day}>
                        {day &&
                            <span className={isEqual(selectedDate, day) ? styles.selected : ''}
                                onClick={() => onChangeDate(day)}>
                                {day.getDate()}
                            </span>
                        }
                    </div>
                ))}
            </div>
        </>
    )
}

MonthPicker.propTypes = {
    days: PropTypes.arrayOf(Date).isRequired,
    selectedDate: PropTypes.instanceOf(Date).isRequired,
    onChangeDate: PropTypes.func.isRequired,
    prevMonth: PropTypes.func.isRequired,
    nextMonth: PropTypes.func.isRequired
}

export default MonthPicker;