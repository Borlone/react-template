import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import styles from "./FormField.module.css";

const FormField = forwardRef(({ className, value, onChange, error, ...rest }, ref) => {

    const { input_field, input_field_error, input_field_input } = styles;

    return (
        <div className={`${input_field} ${className}`}>
            {error?.message && <span className={input_field_error}>{error?.message}</span>}
            <input
                {...rest}
                ref={ref}
                className={input_field_input}
                value={value}
                onChange={onChange}
            />
        </div>
    )
});

FormField.propTypes = {
    className: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};

FormField.defaultProps = {
    className: ""
};

export default FormField;