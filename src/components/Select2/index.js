import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import {
    select2, selectedOption, selectOptions,
    selectOptionsBox, option, show, hidden,
} from './Select2.module.scss'

const Select2 = ({ options, defaultValue, onChange, filterOption, getOptionLabel }) => {

    const [filterOptions, setFilterOptions] = useState([]);
    const [value, setValue] = useState();
    const [showOptions, setShowOptions] = useState(false);

    const handleClick = () => {
        setShowOptions(!showOptions);
    }
    const handleBlur = () => {
        setShowOptions(false);
    }
    const handleFilterOptions = event => {
        setValue(event.target.value)
    }

    let timeout = null;
    useEffect(() => {
        clearTimeout(timeout);

        timeout = setTimeout(() => {
            if (value) {
                const filOptions = options.filter(op => {
                    let 
                    for (let f in filterOption) {
                        op[f]
                    }
                })
            } else {
                setFilterOptions(options)
            }
        }, 500);

        return () => clearTimeout(timeout);
    }, [value])

    const optionLabels = options.map(getOptionLabel);
    return (
        <div className={select2}>
            <div className={selectedOption}>
                <input
                    value={value || defaultValue || ''}
                    onClick={handleClick}
                    onBlur={handleBlur}
                    onChange={handleFilterOptions}
                />
            </div>
            <div className={`${selectOptions} ${showOptions ? show : hidden}`}>
                <div className={selectOptionsBox}>
                    {Array.isArray(optionLabels) && optionLabels.map((o, i) => (
                        <p key={`select2-option-${i}`} className={option}>
                            {o}
                        </p>
                    ))}
                </div>
            </div>
        </div>
    )
}

Select2.propTypes = {
    options: PropTypes.array.isRequired,
    defaultValue: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    filterOption: PropTypes.array.isRequired,
    getOptionLabel: PropTypes.func.isRequired
}
Select2.defaultProps = {
    onChange: () => { },
    getOptionLabel: () => { },
}

export default Select2;