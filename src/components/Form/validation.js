import * as yup from "yup";

const schema = yup.object().shape({
    name: yup.string().required("This field is required."),
    phone: yup.string().required("This field is required.")
        .matches(/\(([0-9]{3})\)\s([0-9]{3})-([0-9]{4})/, 'The phone must contains 10 numbers.')
});

export default schema;