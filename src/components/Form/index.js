import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import schema from './validation';
import FormField from "../FormField";
import NumberFormat from "react-number-format";

const defaultValues = {
    name: '',
    phone: ''
}

const Form = () => {
    const { handleSubmit, reset, control } = useForm({
        resolver: yupResolver(schema)
    });

    const logForm = (data) => {
        console.log(data)
    };

    return (
        <form onSubmit={handleSubmit(logForm)}>

            <Controller
                control={control}
                name='name'
                defaultValue=''
                render={({ field, fieldState }) => (
                    <FormField
                        {...field}
                        error={fieldState.error}
                        placeholder='Name'
                    />
                )}
            />

            <Controller
                control={control}
                name='phone'
                defaultValue=''
                render={({ field, fieldState }) => (
                    <NumberFormat {...field}
                        customInput={FormField}
                        format="(###) ###-####"
                        mask="_"
                        error={fieldState.error}
                        placeholder='Number phone'
                    />
                )}
            />

            <button type="submit">Submit</button>
            <button type='reset' onClick={() => reset({ ...defaultValues })}>Reset</button>
        </form>
    );
};

export default Form;
